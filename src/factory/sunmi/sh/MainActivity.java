/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.myjuniu.factory.sunmi.sh;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import org.apache.cordova.CordovaActivity;

public class MainActivity extends CordovaActivity {
    private Handler mHandler;
    private StringBuffer mStringBufferResult = new StringBuffer();
    public static final int BARCODE_READ = 10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // enable Cordova apps to be started in the background
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("cdvStartInBackground", false)) {
            moveTaskToBack(true);
        }

        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    public void SetHandler(Handler handler) {
        mHandler = handler;
    }

    /**
     * 扫码枪输入事件监听
     *
     * @param event
     * @return
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            handleKeyEvent(event.getKeyCode());
        }
        return super.dispatchKeyEvent(event);
    }

    /**
     * 根据keyCode判断是拼接字符还是返回扫码结果
     *
     * @param keyCode
     */
    private void handleKeyEvent(int keyCode) {
        if (keyCode != KeyEvent.KEYCODE_ENTER) {
            //append char if keycode is not enter
            char inputChar = getInputChar(keyCode);
            mStringBufferResult.append(inputChar);
        } else {
            //keycode是回车键返回当前mStringBufferResult拼接的条码
            if (mHandler != null) {
                String barCode = mStringBufferResult.toString();
                Log.i("123","barCode" + barCode);
                mHandler.obtainMessage(BARCODE_READ, barCode).sendToTarget();
                mStringBufferResult.setLength(0);
            }
        }
    }

    /**
     * 根据keyCode获取输入的字符
     *
     * @param keyCode
     * @return
     */
    private char getInputChar(int keyCode) {
        char inputChar;
        if (keyCode >= KeyEvent.KEYCODE_A && keyCode <= KeyEvent.KEYCODE_Z) {
            //字母
            inputChar = (char) ('A' + keyCode - KeyEvent.KEYCODE_A);
        } else if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
            //数字
            inputChar = (char) ('0' + keyCode - KeyEvent.KEYCODE_0);
        } else {
            //空格
            inputChar = ' ';
        }
        return inputChar;
    }

}
