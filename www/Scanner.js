// cordova.define("cordova-plugin-scanner.Scanner", function (require, exports, module) {
    var exec = require('cordova/exec');

    exports.setScanCallback = function (success) {
        console.log(success)
        exec(success, null, 'Scanner', 'setScanCallback', null);
    };

    exports.startScan = function () {
        exec(null, null, 'Scanner', 'startScan', null);
    };

    exports.stopScan = function () {
        exec(null, null, 'Scanner', 'stopScan', null);
    };

// });
